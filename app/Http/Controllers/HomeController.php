<?php

namespace App\Http\Controllers;

use App\House;
use App\Quoting;
use App\Type;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sales =  Quoting::where ('status','=','VENTA')->get();
        $quotings =  Quoting::where ('status','=','CREADA')->get();
        $types = Type::where ('status','=',1)->get();
        $properties = House::where ('status','=',1)->get();
        return view('home', compact('sales','quotings','types','properties'));
    }

    public function front()
    {
        $properties = House::where ('status','=',1)->take(3)->get();
        return view('front.home', compact('properties'));
    }

    public function about()
    {
        return view('front.about');
    }

    public function thanks()
    {
        return view('front.thank-you');
    }
}

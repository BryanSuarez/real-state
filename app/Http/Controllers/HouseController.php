<?php

namespace App\Http\Controllers;

use App\House;
use App\Http\Requests\HouseFormRequest;
use App\Type;
use Illuminate\Http\Request;

class HouseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('showProperties');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $houses =  House::with('type')
                ->where('title','LIKE','%'.$query.'%')
                ->where ('status','=',1)
                ->orderBy('id','desc')
                ->paginate(5);

            return view('houses.index', compact('houses','query'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::where('status', '=' ,1)->get();
        return view('houses.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseFormRequest $request)
    {
        $house = new House;
        $house->title = $request->get('title');
        $house->location = $request->get('location');
        $house->description = $request->get('description');
        $house->area = $request->get('area');
        $house->bathrooms = $request->get('bathrooms');
        $house->rooms = $request->get('rooms');
        $house->garage = $request->get('garage');
        $house->price = $request->get('price');
        $house->type_id = $request->get('type_id');
        $house->status = $request->get('status');

        if ($request->has('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/propperties/', $filename);
            $house->image = $filename;
            $house->save();
            return redirect()->route('houses.index');
        }else{
            $house->save();
            return redirect()->route('houses.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $house = House::with('type')->findOrFail($id);

        return view('houses.show', compact('house'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types = Type::where('status', '=' ,1)->get();

        $house = House::with('type')->findOrFail($id);

        return view('houses.edit', compact('house','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $house = House::with('type')->findOrFail($id);
        $house->title = $request->get('title');
        $house->location = $request->get('location');
        $house->description = $request->get('description');
        $house->area = $request->get('area');
        $house->bathrooms = $request->get('bathrooms');
        $house->rooms = $request->get('rooms');
        $house->garage = $request->get('garage');
        $house->price = $request->get('price');
        $house->type_id = $request->get('type_id');
        $house->status = $request->get('status');

        if ($request->has('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/propperties/', $filename);
            $house->image = $filename;
            $house->update();
            return redirect()->route('houses.index');
        }else{
            $house->save();
            return redirect()->route('houses.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $house = House::with('type')->findOrFail($id);
        $house->delete();

        return redirect()->route('houses.index');
    }

    public function showProperties()
    {
        $properties = House::with('type')
            ->where('status', '=', 1)
            ->orderBy('id','desc')
            ->get();
        return view('front.properties', compact('properties'));
    }
}

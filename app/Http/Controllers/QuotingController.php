<?php

namespace App\Http\Controllers;

use App\Quoting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class QuotingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $quotings =  Quoting::where('full_name','LIKE','%'.$query.'%')
                ->where ('status','=','CREADA')
                ->orderBy('id','desc')
                ->paginate(5);
            $query=trim($request->get('searchText'));
            $vieweds =  Quoting::where('full_name','LIKE','%'.$query.'%')
                ->where ('status','=','ATENDIDA')
                ->orWhere ('status','=','RECHAZADA')
                ->orderBy('id','desc')
                ->paginate(5);

            $sales =  Quoting::where('full_name','LIKE','%'.$query.'%')
                ->where ('status','=','VENTA')
                ->orderBy('id','desc')
                ->paginate(5);

            return view('quotings.index', compact('quotings','query','vieweds','sales'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quoting = Quoting::findOrFail($id);
        return view('quotings.show', compact('quoting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quoting = Quoting::findOrFail($id);
        return view('quotings.edit', compact('quoting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $quoting = Quoting::findOrFail($id);
        $quoting->final_price = $request->get('final_price');
        $quoting->status = $request->get('status');
        $quoting->update();

        return redirect()->route('quotings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function leadView()
    {
        return view('front.quoting');
    }

    public function createLead(Request $request)
    {
        $quoting = new Quoting;
        $quoting->ci = $request->get('ci');
        $quoting->full_name = $request->get('full_name');
        $quoting->phone = $request->get('phone');
        $quoting->email = $request->get('email');
        $quoting->area = $request->get('area');
        $quoting->budget = $request->get('budget');
        $quoting->property_type = $request->get('property_type');
        $quoting->comment = $request->get('comment');
        $quoting->save();

        $data = array(
            'ci' => $quoting->ci,
            'full_name' => $quoting->full_name,
            'area' => $quoting->area,
            'budget' => $quoting->budget,
            'property_type' => $quoting->property_type,
            'comment' => $quoting->comment,
        );

        Mail::send('emails.quoting', $data, function ($message) use( $quoting){
            $message->from($quoting->email, 'Nueva cotizacion');
            $message->to('admin@admin.com')->subject('Asunto');
        });

        return view('front.thank-you', compact('quoting'));
    }
}

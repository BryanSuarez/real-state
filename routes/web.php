<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@front');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/properties', 'HouseController@showProperties')->name('properties');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/thank-you', 'HomeController@thanks')->name('thanks');

Route::resource('admin/quotings','QuotingController');
Route::get('quotings','QuotingController@leadView')->name('lead-view');
Route::post('quotings','QuotingController@createLead')->name('lead-store');
Route::resource('admin/houses','HouseController');
Route::resource('admin/types','TypeController');

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void,
     */
    public function up()
    {
        Schema::create('quotings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ci');
            $table->string('full_name');
            $table->string('phone');
            $table->string('email');
            $table->integer('area');
            $table->integer('budget');
            $table->string('property_type');
            $table->text('comment')->nullable();
            $table->double('final_price')->nullable();
            $table->string('status')->default('CREADA');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotings');
    }
}

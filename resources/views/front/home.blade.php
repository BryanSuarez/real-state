@extends('front.master')

@section('front-content')
    <!-- Home -->

    <div class="home" style="height: 100vh">

        <!-- Home Slider -->
        <div class="home_slider_container">
            <div class="owl-carousel owl-theme home_slider">

                <!-- Slide -->
                <div class="slide">
                    <div class="background_image" style="background-image:url('{{ asset('front/images/index.jpg') }}')"></div>
                    <div class="home_container">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="home_content">
                                        <div class="home_title"><h1>Casa Master - Quito</h1></div>
                                        <div class="home_price_tag">$ 482 900</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slide -->
                @foreach($properties as $property)
                    <div class="slide">
                        <div class="background_image" style="width: 1920px; height: 947px; max-width: 100%; background-image:url('{!! url('/uploads/propperties/'.$property->image) !!}')"></div>
                        <div class="home_container">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="home_content">
                                            <div class="home_title"><h1>{{ $property->title }} - {{ $property->location }}</h1></div>
                                            <div class="home_price_tag">${{ $property->price }}</div>
                                            <p class="p-2 text-white">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos doloribus esse ex inventore omnis ratione!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach



            </div>

            <!-- Home Slider Navigation -->
            <div class="home_slider_nav"><i class="fa fa-angle-right" aria-hidden="true"></i></div>

        </div>
    </div>

    <!-- Featured -->

    <div class="featured">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <div class="section_subtitle">las mejores opciones</div>
                        <div class="section_title"><h1>Propiedades destacadas</h1></div>
                    </div>
                </div>
            </div>
            <br>
            <div class="listings">
                <div class="container">
                    <div class="row">
                        <div class="col">

                            <!-- Listings Container -->
                            <div class="listings_container" style="display: flex; flex-wrap: wrap; margin-top: -1rem">

                                <!-- PRODUCT  -->
                                @foreach($properties as $property)
                                    <div class="listing_box house sale">
                                        <div class="listing">
                                            <div class="listing_image">
                                                <div class="listing_image_container">
                                                    @if($property->image)
                                                        <img src="{!! url('/uploads/propperties/'.$property->image) !!}" width="350" height="270" alt="{{ $property->title }}">
                                                    @else
                                                        <img src="{{ asset('front/images/listing_1.jpg') }}" alt="">
                                                    @endif
                                                </div>
                                                <div class="tags d-flex flex-row align-items-start justify-content-start flex-wrap">
                                                    @if($property->type->id == 1)
                                                        <div class="tag tag_house"><a href="javascript:void(0)">{{ $property->type->name }}</a></div>
                                                    @else
                                                        <div class="tag tag_sale"><a href="javascript:void(0)">{{ $property->type->name }}</a></div>
                                                    @endif
                                                </div>
                                                <div class="tag_price listing_price">${{ $property->price }}</div>
                                            </div>
                                            <div class="listing_content">
                                                <div class="prop_location listing_location d-flex flex-row align-items-start justify-content-start">
                                                    <img src="images/icon_1.png" alt="">
                                                    <a href="single.html">{{ $property->title }}</a>
                                                </div>
                                                <div class="listing_info">
                                                    <ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
                                                        <li class="property_area d-flex flex-row align-items-center justify-content-start">
                                                            <img src="{{ asset('front/images/icon_2.png') }}" alt="">
                                                            <span>{{ $property->area }} m2</span>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <img src="{{ asset('front/images/icon_3.png') }}" alt="">
                                                            <span>{{ $property->bathrooms }}</span>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <img src="{{ asset('front/images/icon_4.png') }}" alt="">
                                                            <span>{{ $property->rooms }}</span>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <img src="{{ asset('front/images/icon_5.png') }}" alt="">
                                                            <span>{{ $property->garage }}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach

                            <!--/ PRODUCT -->

                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <!-- Map Section -->

    <!-- Hot -->


    <!-- Testimonials -->

    <div class="testimonials container_reset">
        <div class="container">
            <div class="row row-eq-height">

                <!-- Testimonials Image -->
                <div class="col-xl-6">
                    <div class="testimonials_image">
                        <div class="background_image" style="background-image:url('{{ asset('front/images/testimonials.jpg') }}')"></div>
                        <div class="testimonials_image_overlay"></div>
                    </div>
                </div>

                <!-- Testimonials Content -->
                <div class="col-xl-6">
                    <div class="testimonials_content">
                        <div class="section_title_container">
                            <div class="section_subtitle">Los mejores diseños</div>
                            <div class="section_title"><h1>Testimonios de kathy</h1></div>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis enim exercitationem iusto quasi temporibus totam ut? Beatae cumque ea eaque facere incidunt laboriosam, non odit placeat quae tenetur! Inventore, vero.
                            </p>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

@endsection

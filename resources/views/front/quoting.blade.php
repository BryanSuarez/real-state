@include('front.partials.header')


<br>
<div class="featured">
    <div class="row">
        <div class="container">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col">
                        <div class="section_title_container text-center">
                            <div class="section_title"><h1>Cotiza tu propiedad</h1></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi debitis dolorem eaque eligendi error excepturi exercitationem fugit illum molestias nam nostrum odit omnis perferendis, quisquam rem sapiente sint veniam voluptatum!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <br>
                <form action="{{ route('lead-store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="ci">Cedula</label>
                                <input type="text" required class="form-control" placeholder="Ingresa tu cedula" name="ci">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="full_name">Nombre y apellido</label>
                                <input type="text" required class="form-control" placeholder="Ingresa tu nombre completo" name="full_name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="phone">Telefono</label>
                                <input type="tel" required class="form-control" placeholder="Ingresa tu numero telefonico" name="phone">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" required class="form-control" placeholder="Ingresa tu email" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="area">Superfices</label>
                                <input type="number" required class="form-control" placeholder="Ingresa la superficie de tu terreno" name="area">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="budget">Presupuesto</label>
                                <input type="number" required class="form-control" placeholder="Ingresa tu presupuesto estimado" name="budget">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="property_type">Tipo de propiedad</label>
                                <select name="property_type" required class="form-control">
                                    <option value="0" disabled selected>Selecciona</option>
                                    <option value="una planta">Una planta</option>
                                    <option value="dos plantas">Dos plantas</option>
                                    <option value="quinta">Quinta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="comment">Dejanos un comentario breve</label>
                                <textarea name="comment" cols="30" rows="5" class="form-control" placeholder="Dinos algo mas sobre tu cotizacion"></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 col-md-3 text-center">
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Cotizar</button>
                            </div>
                        </div>
                    </div>
                </form>
                <br><br>
            </div>
        </div>
    </div>
</div>

@include('front.partials.footer')

@extends('front.master')

@section('front-content')

    <!-- Home -->
    <br><br>
    <div class="home" style="height: 250px;">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('front/images/about.jpg') }}" data-speed="0.8"></div>
        <div class="home_contents text-center"><br><br>
            <div class="home_title">Gracias por contactarnos</div>
        </div>
    </div>

    <br><br><br><br><br><br><br>


    <!-- Listings -->

    <div class="listings">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3 class="text-center">Hola <strong>{{ $quoting->full_name }}</strong></h3>
                    <hr>
                    <div class="container">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aspernatur assumenda dicta dolorum eligendi esse laboriosam necessitatibus, nobis nostrum quas quisquam sed sit soluta ullam unde voluptate voluptatem voluptates!
                        </p>
                        <p>
                            En breve uno de nuestros asesores atenderá tu requerimiento.
                        </p>
                    </div>
                    <br>
                    <center>
                        <a href="{{ route('properties') }}" class="search_button p-2 text-center">Mira nuestros productos terminados</a>
                    </center>
                    <br>

                    <hr>

                </div>
            </div>
        </div>
        <br><br>

@endsection


@extends('front.master')

@section('front-content')

    <!-- Home -->
    <br><br>
    <div class="home" style="height: 250px;">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('front/images/about.jpg') }}" data-speed="0.8"></div>
        <div class="home_contents text-center">
            <div class="home_title">Sobre Nosotros</div>
        </div>
    </div>

    <!-- Intro -->

    <div class="intro">
        <div class="container">
            <div class="row row-eq-height">

                <!-- Intro Content -->
                <div class="col-lg-6">
                    <div class="intro_content">
                        <div class="section_title_container">
                            <div class="section_subtitle">Los mejores profesionales</div>
                            <div class="section_title"><h1>¿Quiénes Somos?</h1></div>
                        </div>
                        <div class="intro_text">
                            <p>Nulla aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula urna. Suspendisse fringilla lobortis justo, ut tempor leo cursus in. Nulla aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula urna. Suspendisse fringilla lobortis justo, ut tempor leo cursus in.</p>
                        </div>
                        <div class="button intro_button"><a href="#">read more</a></div>
                    </div>
                </div>

                <!-- Intro Image -->
                <div class="col-lg-6 intro_col">
                    <div class="intro_image">
                        <div class="background_image" style="background-image:url({{ asset('front/images/intro.jpg') }})"></div>
                        <img src="images/intro.jpg" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Services -->

    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <div class="section_subtitle">Somos los mejores</div>
                        <div class="section_title"><h1>Nuestros servicios</h1></div>
                    </div>
                </div>
            </div>
            <div class="row services_row">

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_1.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 1</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Nulla aliquet bibendum sem, non placer risus venenatis at. Prae sent vulputate bibendum dictum.</p>
                        </div>
                    </div>
                </div>

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_2.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 2</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula.</p>
                        </div>
                    </div>
                </div>

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_3.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 3</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Nulla aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum.</p>
                        </div>
                    </div>
                </div>

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_4.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 4</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula.</p>
                        </div>
                    </div>
                </div>

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_5.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 6</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula.</p>
                        </div>
                    </div>
                </div>

                <!-- Service -->
                <div class="col-xl-4 col-md-6">
                    <div class="service">
                        <div class="service_title_container d-flex flex-row align-items-center justify-content-start">
                            <div class="service_icon d-flex flex-column align-items-start justify-content-center">
                                <img src="{{ asset('front/images/service_6.png') }}" alt="">
                            </div>
                            <div class="service_title"><h3>Servicio 6</h3></div>
                        </div>
                        <div class="service_text">
                            <p>Aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Milestones -->

    <div class="milestones">
        <div class="container">
            <div class="row">

                <!-- Milestone -->
                <div class="col-xl-3 col-md-6 milestone_col">
                    <div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
                        <div class="milestone_content">
                            <div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="{{ asset('front/images/duplex.svg') }}" alt=""></div>
                            <div class="milestone_counter" data-end-value="25">0</div>
                            <div class="milestone_text">casas construidas</div>
                        </div>
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-xl-3 col-md-6 milestone_col">
                    <div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
                        <div class="milestone_content">
                            <div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="{{ asset('front/images/prize.svg') }}" alt=""></div>
                            <div class="milestone_counter" data-end-value="18">0</div>
                            <div class="milestone_text">reconocimientos</div>
                        </div>
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-xl-3 col-md-6 milestone_col">
                    <div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
                        <div class="milestone_content">
                            <div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="{{ asset('front/images/home.svg') }}" alt=""></div>
                            <div class="milestone_counter" data-end-value="25" data-sign-after="k">0</div>
                            <div class="milestone_text">seguidores</div>
                        </div>
                    </div>
                </div>

                <!-- Milestone -->
                <div class="col-xl-3 col-md-6 milestone_col">
                    <div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
                        <div class="milestone_content">
                            <div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="{{ asset('front/images/rent.svg') }}" alt=""></div>
                            <div class="milestone_counter" data-end-value="1265">0</div>
                            <div class="milestone_text">alquileres</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Agents -->

    <div class="agents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container text-center">
                        <div class="section_title"><h1>Nuestro Equipo</h1></div>
                    </div>
                </div>
            </div>
            <div class="row agents_row">

                <!-- Agent -->
                <div class="col-lg-4 agent_col">
                    <div class="agent">
                        <div class="agent_image"><img src="{{ asset('front/images/realtor_1.jpg') }}" alt=""></div>
                        <div class="agent_content">
                            <div class="agent_name"><a href="#">Nombre Apellido</a></div>
                            <div class="agent_title">El Cargo</div>
                            <div class="agent_list">
                                <ul>
                                    <li>alguien@myhometemp.com</li>
                                    <li>+593 333 999</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Agent -->
                <div class="col-lg-4 agent_col">
                    <div class="agent">
                        <div class="agent_image"><img src="{{ asset('front/images/realtor_2.jpg') }}" alt=""></div>
                        <div class="agent_content">
                            <div class="agent_name"><a href="#">Nombre Apellido</a></div>
                            <div class="agent_title">El Cargo</div>
                            <div class="agent_list">
                                <ul>
                                    <li>alguien@myhometemp.com</li>
                                    <li>+45 27774 5653 267</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Agent -->
                <div class="col-lg-4 agent_col">
                    <div class="agent">
                        <div class="agent_image"><img src="{{ asset('front/images/realtor_3.jpg') }}" alt=""></div>
                        <div class="agent_content">
                            <div class="agent_name"><a href="#">Nombre Apellido</a></div>
                            <div class="agent_title">El Cargo</div>
                            <div class="agent_list">
                                <ul>
                                    <li>alguien@myhometemp.com</li>
                                    <li>+45 27774 5653 267</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row contact_row">
                <div class="col">
                    <div class="button ml-auto mr-auto"><a href="#">Haz tu cotización</a></div>
                </div>
            </div>
        </div>
    </div>


@endsection


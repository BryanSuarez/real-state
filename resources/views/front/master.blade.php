<!DOCTYPE html>
<html lang="en">
<head>
    <title>CotizApp</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Cotizador web">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/bootstrap-4.1.2/bootstrap.min.css') }}">
    <link href="{{ asset('front/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.3.4/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.3.4/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.3.4/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/main_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/about.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/about_responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/listings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/styles/listings_responsive.css') }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>

<div class="super_container">
    <div class="super_overlay"></div>

    <!-- Header -->

    <header class="header">

        <!-- Header Bar -->
        <div class="header_bar d-flex flex-row align-items-center justify-content-start">
            <div class="header_list">
                <ul class="d-flex flex-row align-items-center justify-content-start">
                    <!-- Phone -->
                    <li class="d-flex flex-row align-items-center justify-content-start">
                        <div><img src="{{asset('front/images/phone-call.svg')}}" alt=""></div>
                        <span><a href="tel:0999999999">+593 990221 123</a></span>
                    </li>
                    <!-- Address -->
                    <li class="d-flex flex-row align-items-center justify-content-start">
                        <div><img src="{{asset('front/images/placeholder.svg')}}" alt=""></div>
                        <span>C/ Amazonas, Tumbaco</span>
                    </li>
                    <!-- Email -->
                    <li class="d-flex flex-row align-items-center justify-content-start">
                        <div><img src="{{asset('front/images/envelope.svg')}}" alt=""></div>
                        <span>hosting@contact.com</span>
                    </li>
                </ul>
            </div>
            <div class="ml-auto d-flex flex-row align-items-center justify-content-start">

                <div class="log_reg d-flex flex-row align-items-center justify-content-start">
                    <ul class="d-flex flex-row align-items-start justify-content-start">
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Regístrate</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Header Content -->
        <div class="header_content d-flex flex-row align-items-center justify-content-start">
            <div class="logo"><a href="#">Cotiz<span>App</span></a></div>
            <nav class="main_nav">
                <ul class="d-flex flex-row align-items-start justify-content-start">
                    <li><a href="/">Inicio</a></li>
                    <li><a href="{{ route('about') }}">Sobre Nosotros</a></li>
                    <li><a href="{{ route('properties') }}">Productos</a></li>
                </ul>
            </nav>
            <div class="submit ml-auto"><a href="{{ route('lead-view') }}">Cotizador</a></div>
            <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
        </div>

    </header>

    <!-- Menu -->

    <div class="menu text-right">
        <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="menu_log_reg">
            <div class="log_reg d-flex flex-row align-items-center justify-content-end">
                <ul class="d-flex flex-row align-items-start justify-content-start">
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Regístrate</a></li>
                </ul>
            </div>
            <nav class="menu_nav">
                <ul>
                    <li><a fhref="/">Inicio</a></li>
                    <li><a href="#">Sobre Nosotros</a></li>
                    <li><a href="#">Productos</a></li>
                </ul>
            </nav>
        </div>
    </div>

    @yield('front-content')

    <footer class="footer">
        <div class="footer_content">
            <div class="container">
                <div class="row">

                    <!-- Footer Column -->
                    <div class="col-xl-3 col-lg-6 footer_col">
                        <div class="footer_about">
                            <div class="footer_logo"><a href="#">Cotiz<span>App</span></a></div>
                            <div class="footer_text">
                                <p>Nulla aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula urna. Suspendisse fringilla lobortis justo, ut tempor leo cursus in.</p>
                            </div>
                            <div class="social">
                                <ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="footer_submit"><a href="#">Cotizador</a></div>
                        </div>
                    </div>

                    <!-- Footer Column -->
                    <div class="col-xl-3 col-lg-6 footer_col">
                        <div class="footer_column">
                            <div class="footer_title">Información</div>
                            <div class="footer_info">
                                <ul>
                                    <!-- Phone -->
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div><img src="{{ asset('front/images/phone-call.svg') }}" alt=""></div>
                                        <span>+593 999 999 999</span>
                                    </li>
                                    <!-- Address -->
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div><img src="{{ asset('front/images/placeholder.svg') }}" alt=""></div>
                                        <span>Tumbaco, Quito - Ecuador</span>
                                    </li>
                                    <!-- Email -->
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <div><img src="{{ asset('front/images/envelope.svg') }}" alt=""></div>
                                        <span>hosting@contact.com</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer_links usefull_links">
                                <div class="footer_title">Enlaces útiles</div>
                                <ul>
                                    <li><a href="#">Testimonios</a></li>
                                    <li><a href="#">Cotizador</a></li>
                                    <li><a href="#">Propiedades Destacadas</a></li>
                                    <li><a href="#">Contactos</a></li>
                                    <li><a href="#">Sobre <Nosotros></Nosotros></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Footer Column -->
                    <div class="col-xl-3 col-lg-6 footer_col">
                        <div class="footer_links">
                            <div class="footer_title">Tipos de Propiedades</div>
                            <ul>
                                <li><a href="#">Alquiler</a></li>
                                <li><a href="#">Venta</a></li>
                                <li><a href="#">Diseños personalizados</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Footer Column -->
                    <div class="col-xl-3 col-lg-6 footer_col">
                        <div class="footer_title">Producto destacado</div>
                        <div class="listing_small">
                            <div class="listing_small_image">
                                <div>
                                    <img src="{{ asset('front/images/listing_1.jpg') }}" alt="">
                                </div>
                                <div class="listing_small_tags d-flex flex-row align-items-start justify-content-start flex-wrap">
                                    <div class="listing_small_tag tag_house"><a href="#">terminados</a></div>
                                    <div class="listing_small_tag tag_sale"><a href="#">cotizador</a></div>
                                </div>
                                <div class="listing_small_price">$ 250 000</div>
                            </div>
                            <div class="listing_small_content">
                                <div class="listing_small_location d-flex flex-row align-items-start justify-content-start">
                                    <img src="{{ asset('front/images/icon_1.png') }}" alt="">
                                    <a href="single.html">N280-E12 Cumbaya - Quito</a>
                                </div>
                                <div class="listing_small_info">
                                    <ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                            <img src="{{ asset('front/images/icon_3.png') }}" alt="">
                                            <span>2</span>
                                        </li>
                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                            <img src="{{ asset('front/images/icon_4.png') }}" alt="">
                                            <span>5</span>
                                        </li>
                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                            <img src="{{ asset('front/images/icon_5.png') }}" alt="">
                                            <span>2</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer_bar">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer_bar_content d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start">
                            <div class="copyright order-md-1 order-2">
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> Derechos reservados |
                                Desarrollado
                                </i> por <a href="#" target="_blank">Katherin Tigasi</a>
                            </div>
                            <nav class="footer_nav order-md-2 order-1 ml-md-auto">
                                <ul class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start">
                                    <li><a href="/">Inicio</a></li>
                                    <li><a href="{{ route('about') }}">Sobre Nosotros</a></li>
                                    <li><a href="{{ route('properties') }}">Productos</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="{{ asset('front/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap-4.1.2/popper.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap-4.1.2/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('front/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('front/plugins/OwlCarousel2-2.3.4/owl.carousel.js') }}"></script>
<script src="{{ asset('front/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('front/plugins/progressbar/progressbar.min.js') }}"></script>
<script src="{{ asset('front/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="{{ asset('front/js/custom.js') }}"></script>
<script src="{{ asset('front/js/about.js') }}"></script>
<script src="{{ asset('front/js/listings.js') }}"></script>

</body>
</html>


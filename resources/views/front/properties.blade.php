@extends('front.master')

@section('front-content')

    <!-- Home -->
    <br><br>
    <div class="home" style="height: 250px;">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('front/images/about.jpg') }}" data-speed="0.8"></div>
        <div class="home_contents text-center"><br><br>
            <div class="home_title">Propiedades</div>
        </div>
    </div>

    <br><br><br><br><br><br><br><br><br>

    <!-- Search -->

    <div class="search">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="search_container">
                        <div class="search_form_container">
                            <form action="#" class="search_form" id="search_form">
                                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">
                                    <div class="search_inputs d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">
                                        <p class="text-white text-center">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam inventore provident quo repellendus. Aliquid ea earum eius harum hic ipsa itaque molestiae odio qui quisquam reiciendis rerum soluta suscipit, tempora.
                                        </p>
                                    </div>
                                    <h4 class="text-white">Lorem ipsum dolor sit.</h4>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Listings -->

    <div class="listings">
        <div class="container">
            <div class="row">
                <div class="col">

                    <!-- Listings Container -->
                    <div class="listings_container" style="display: flex; flex-wrap: wrap; margin-top: -1rem">

                        <!-- PRODUCT  -->
                        @foreach($properties as $property)
                            <div class="listing_box house sale">
                                <div class="listing">
                                    <div class="listing_image">
                                        <div class="listing_image_container">
                                            @if($property->image)
                                                <img src="{!! url('/uploads/propperties/'.$property->image) !!}" width="350" height="270" alt="{{ $property->title }}">
                                            @else
                                                <img src="{{ asset('front/images/listing_1.jpg') }}" alt="">
                                            @endif
                                        </div>
                                        <div class="tags d-flex flex-row align-items-start justify-content-start flex-wrap">
                                            @if($property->type->id == 1)
                                                <div class="tag tag_house"><a href="javascript:void(0)">{{ $property->type->name }}</a></div>
                                            @else
                                                <div class="tag tag_sale"><a href="javascript:void(0)">{{ $property->type->name }}</a></div>
                                            @endif
                                        </div>
                                        <div class="tag_price listing_price">${{ $property->price }}</div>
                                    </div>
                                    <div class="listing_content">
                                        <div class="prop_location listing_location d-flex flex-row align-items-start justify-content-start">
                                            <img src="images/icon_1.png" alt="">
                                            <a href="single.html">{{ $property->title }}</a>
                                        </div>
                                        <div class="listing_info">
                                            <ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
                                                <li class="property_area d-flex flex-row align-items-center justify-content-start">
                                                    <img src="{{ asset('front/images/icon_2.png') }}" alt="">
                                                    <span>{{ $property->area }} m2</span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <img src="{{ asset('front/images/icon_3.png') }}" alt="">
                                                    <span>{{ $property->bathrooms }}</span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <img src="{{ asset('front/images/icon_4.png') }}" alt="">
                                                    <span>{{ $property->rooms }}</span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <img src="{{ asset('front/images/icon_5.png') }}" alt="">
                                                    <span>{{ $property->garage }}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <!--/ PRODUCT -->

                </div>
            </div>
        </div>
    </div>





@endsection


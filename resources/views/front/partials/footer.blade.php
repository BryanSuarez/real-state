
<footer class="footer">
    <div class="footer_content">
        <div class="container">
            <div class="row">

                <!-- Footer Column -->
                <div class="col-xl-3 col-lg-6 footer_col">
                    <div class="footer_about">
                        <div class="footer_logo"><a href="#">Cotiz<span>App</span></a></div>
                        <div class="footer_text">
                            <p>Nulla aliquet bibendum sem, non placerat risus venenatis at. Prae sent vulputate bibendum dictum. Cras at vehicula urna. Suspendisse fringilla lobortis justo, ut tempor leo cursus in.</p>
                        </div>
                        <div class="social">
                            <ul class="d-flex flex-row align-items-center justify-content-start">
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="footer_submit"><a href="#">Cotizador</a></div>
                    </div>
                </div>

                <!-- Footer Column -->
                <div class="col-xl-3 col-lg-6 footer_col">
                    <div class="footer_column">
                        <div class="footer_title">Información</div>
                        <div class="footer_info">
                            <ul>
                                <!-- Phone -->
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div><img src="{{ asset('front/images/phone-call.svg') }}" alt=""></div>
                                    <span>+593 999 999 999</span>
                                </li>
                                <!-- Address -->
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div><img src="{{ asset('front/images/placeholder.svg') }}" alt=""></div>
                                    <span>Tumbaco, Quito - Ecuador</span>
                                </li>
                                <!-- Email -->
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div><img src="{{ asset('front/images/envelope.svg') }}" alt=""></div>
                                    <span>hosting@contact.com</span>
                                </li>
                            </ul>
                        </div>
                        <div class="footer_links usefull_links">
                            <div class="footer_title">Enlaces útiles</div>
                            <ul>
                                <li><a href="#">Testimonios</a></li>
                                <li><a href="#">Cotizador</a></li>
                                <li><a href="#">Propiedades Destacadas</a></li>
                                <li><a href="#">Contactos</a></li>
                                <li><a href="#">Sobre <Nosotros></Nosotros></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Footer Column -->
                <div class="col-xl-3 col-lg-6 footer_col">
                    <div class="footer_links">
                        <div class="footer_title">Tipos de Propiedades</div>
                        <ul>
                            <li><a href="#">Alquiler</a></li>
                            <li><a href="#">Venta</a></li>
                            <li><a href="#">Diseños personalizados</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Footer Column -->
                <div class="col-xl-3 col-lg-6 footer_col">
                    <div class="footer_title">Producto destacado</div>
                    <div class="listing_small">
                        <div class="listing_small_image">
                            <div>
                                <img src="{{ asset('front/images/listing_1.jpg') }}" alt="">
                            </div>
                            <div class="listing_small_tags d-flex flex-row align-items-start justify-content-start flex-wrap">
                                <div class="listing_small_tag tag_house"><a href="#">terminados</a></div>
                                <div class="listing_small_tag tag_sale"><a href="#">cotizador</a></div>
                            </div>
                            <div class="listing_small_price">$ 250 000</div>
                        </div>
                        <div class="listing_small_content">
                            <div class="listing_small_location d-flex flex-row align-items-start justify-content-start">
                                <img src="{{ asset('front/images/icon_1.png') }}" alt="">
                                <a href="single.html">N280-E12 Cumbaya - Quito</a>
                            </div>
                            <div class="listing_small_info">
                                <ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <img src="{{ asset('front/images/icon_3.png') }}" alt="">
                                        <span>2</span>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <img src="{{ asset('front/images/icon_4.png') }}" alt="">
                                        <span>5</span>
                                    </li>
                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                        <img src="{{ asset('front/images/icon_5.png') }}" alt="">
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="footer_bar">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer_bar_content d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start">
                        <div class="copyright order-md-1 order-2">
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> Derechos reservados |
                            Desarrollado con <i class="fa fa-heart-o" aria-hidden="true">
                            </i> por <a href="#" target="_blank">Katherin Tigasi</a>
                            </div>
                        <nav class="footer_nav order-md-2 order-1 ml-md-auto">
                            <ul class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start">
                                <li><a href="/">Inicio</a></li>
                                <li><a href="#">Sobre Nosotros</a></li>
                                <li><a href="#">Productos</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contactos</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<script src="{{ asset('front/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap-4.1.2/popper.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap-4.1.2/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('front/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('front/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('front/plugins/OwlCarousel2-2.3.4/owl.carousel.js') }}"></script>
<script src="{{ asset('front/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('front/plugins/progressbar/progressbar.min.js') }}"></script>
<script src="{{ asset('front/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="{{ asset('front/js/custom.js') }}"></script>
<script src="{{ asset('front/js/about.js') }}"></script>
</body>
</html>

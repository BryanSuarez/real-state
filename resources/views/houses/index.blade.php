@extends('layouts.master')

@section('contents')

    @include('houses.partials.header')

    <div class="row">
        <div class="col-12">
            @include('houses.partials.table')
        </div>
    </div>



@endsection

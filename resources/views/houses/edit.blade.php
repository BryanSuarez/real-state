@extends('layouts.master')

@section('contents')

@include('houses.partials.header')

<div class="row">
    <div class="col-12">
        <form action="{{ route('houses.update', $house->id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Título:</label>
                        <input type="text" value="{{ $house->title }}" class="form-control" name="title" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="location">Ubicación</label>
                        <input type="text" class="form-control" required name="location" value="{{ $house->location }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="area">Superficie</label>
                        <input type="number" class="form-control" required name="area" value="{{ $house->area }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="rooms">Dormitorios</label>
                        <input type="number" class="form-control" required name="rooms" placeholder="2" value="{{ $house->rooms }}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bathrooms">Baños</label>
                        <input type="number" class="form-control" required placeholder="1" name="bathrooms" value="{{ $house->bathrooms }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="garage">Garage</label>
                        <input type="number" class="form-control" required placeholder="2" name="garage" value="{{ $house->garage }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="price">Precio</label>
                        <input type="number" class="form-control" required placeholder="25000" name="price" value="{{ $house->price }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="image">Imagen principal</label>
                        @if(($house->image) != "")
                            <img src="{!! url('/uploads/propperties/'.$house->image) !!}" alt="{{ $house->title }}" width="50" height="50" class="img-responsive img-thumbnail">
                        @endif
                        <input type="file" name="image" class="form-control" >
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label for="type_id">Tipo de propiedad</label>
                        <select name="type_id" class="form-control">
                            <option value="" selected disabled>Seleccione...</option>
                            @foreach($types as $type)
                                @if($type->type_id != $house->id)
                                    <option selected value="{{ $type->id }}">{{ $type->name }}</option>
                                @else
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="status">Estado</label>
                        <select name="status" class="form-control">
                            <option value="1" selected>Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <textarea name="description" cols="30" rows="3" class="form-control" placeholder="Ingrese una breve descripción" >{{ $house->description }}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <a href="{{ route('houses.index') }}" class="btn btn-secondary mr-2" >Cancelar</a>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



@endsection

<!-- Modal -->
<div class="modal fade" id="houseDeleteModal-{{ $house->id }}" tabindex="-1" role="dialog" aria-labelledby="houseDeleteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="houseCreateModalLabel">Eliminar propiedad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('houses.destroy', $house->id) }}" method="POST">
                {{ csrf_field() }}
                @method('DELETE')
                <div class="modal-body">
                    <div class="form-group">
                        <p>
                            ¿Desea eliminar el registro?: <strong> {{ $house->title }} </strong>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                </div>
            </form>
        </div>
    </div>
</div>

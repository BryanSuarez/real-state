<div class="container">

    <div class="details" style="display: flex; flex-wrap: wrap; justify-content: space-between;">
        <div class="info">
            <p class="text-muted">
                <strong>Propiedad:</strong> {{ $house->title }}
            </p>
            <p class="text-muted">
                <strong>Ubicación:</strong> {{ $house->location }}
            </p>

            <p class="text-muted">
                <strong>Superficie:</strong> {{ $house->area }} m2
            </p>

            <p class="text-muted">
                <strong>N° de dormitorios:</strong> {{ $house->rooms }}
            </p>

            <p class="text-muted">
                <strong>N° de baños:</strong> {{ $house->bathrooms }}
            </p>

            <p class="text-muted">
                <strong>N° de garages:</strong> {{ $house->garage }}
            </p>

            <p class="text-muted">
                <strong>Precio</strong> {{ $house->price }}
            </p>

            <p class="text-muted">
                <strong>Descripción:</strong> {{ $house->description }}
            </p>
        </div>

        <div class="image">
            <p class="text-muted text-center">
                <strong>Imagen principal</strong>
            </p>
            @if($house->image)
                <img src="{!! url('/uploads/propperties/'.$house->image) !!}" alt="{{ $house->title }}" width="250" height="250" class="img-responsive">
            @else
                <span class="text-danger"> Aún no se ha asignado imagen principal</span>
            @endif
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-sm-12 col-md-5">
            <a href="{{ route('houses.index') }}" class="btn btn-secondary mr-2" >Regresar</a>
        </div>
    </div>

</div>

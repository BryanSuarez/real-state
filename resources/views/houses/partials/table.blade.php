<!-- Disparar odal
<button type="button" class="btn btn-outline-dark my-2" data-toggle="modal" data-target="#houseCreateModal">
    Añadir propiedad
</button>
-->

<a href="{{ route('houses.create') }}" class="btn btn-outline-dark my-2">Añadir propiedad</a>

<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Gestión de Propiedades</h3>

        <div class="card-tools">
            <form action="{{ route('houses.index') }}" method="GET">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="searchText" class="form-control float-right" placeholder="Buscar registro"
                           value="{{ $query }}">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-light"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Ubicación</th>
            <th>Tipo de propiedad</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @foreach($houses as $house)
                <tr>
                    <td>{{ $house->id }}</td>
                    <td>{{ $house->title }}</td>
                    <td>{{ $house->location }}</td>
                    <td>{{ $house->type->name }}</td>
                    <td>
                        <a href="{{ route('houses.show', $house->id) }}" class="btn btn-outline-info">Detalles</a>
                        <a href="{{ route('houses.edit', $house->id) }}" class="btn btn-primary">Editar</a>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#houseDeleteModal-{{ $house->id }}">
                            Eliminar
                        </button>
                    </td>
                    @include('houses.partials.delete')
                </tr>
            @endforeach
            </tbody>
        </table>
        @include('houses.partials.modal-create')
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        {{ $houses->links() }}
    </div>
</div>
<!-- /.card -->

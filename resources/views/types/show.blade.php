@extends('layouts.master')

@section('contents')

    @include('types.partials.header')

    <div class="row">
        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Detalles de Categoría</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <br>
                    @include('types.partials.details')
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection



<!-- Disparar odal -->
<button type="button" class="btn btn-outline-dark my-2" data-toggle="modal" data-target="#typeCreateModal">
    Añadir nueva
</button>
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Gestión de Categorías</h3>

        <div class="card-tools">
            <form action="{{ route('types.index') }}" method="GET">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="searchText" class="form-control float-right" placeholder="Buscar registro"
                           value="{{ $query }}">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-light"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Estado</th>
            <th>Acciones</th>
            </thead>
            <tbody>
            @foreach($types as $type)
                <tr>
                    <td>{{ $type->id }}</td>
                    <td>{{ $type->name }}</td>
                    <td>Activo</td>
                    <td>
                        <a href="{{ route('types.show', $type->id) }}" class="btn btn-outline-info">Detalles</a>
                        <a href="{{ route('types.edit', $type->id) }}" class="btn btn-primary">Editar</a>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#typeDeleteModal-{{ $type->id }}">
                            Eliminar
                        </button>
                    </td>

                    @include('types.partials.delete')
                </tr>
            @endforeach
            </tbody>
        </table>
        @include('types.partials.modal-create')
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        {{ $types->links() }}
    </div>
</div>
<!-- /.card -->

<form action="{{ route('types.update',$type->id) }}" method="POST">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}

    <div class="container">
        <div class="form-group">
            <label for="name">Nombre:</label>
            <input type="text" placeholder="Nombre de la categoría" class="form-control" name="name" required
                   value="{{ $type->name }}">
        </div>

        <div class="form-group">
            <a href="{{ route('types.index') }}" class="btn btn-danger">Cancelar</a>
            <button type="submit" class="btn btn-primary">Registrar</button>
        </div>
    </div>
</form>

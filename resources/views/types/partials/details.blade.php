<div class="container">
    <p>Nombre de la categoría: <b>{{ $type->name }}</b></p>
    <p>Estado de la categoría: <b class="label label-success p-1 rounded">Activo</b></p>
    <hr>

    <a href="{{ route('types.index') }}" class="btn btn-outline-dark m-2">Volver</a><br>
</div>

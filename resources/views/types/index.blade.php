@extends('layouts.master')

@section('contents')

    @include('types.partials.header')

    <div class="row">
        <div class="col-12">
            @include('types.partials.table')
        </div>
    </div>



@endsection

<div class="card-body table-responsive p-0">
    <table class="table table-hover table-bordered">
        <thead>
        <th>ID</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Email</th>
        <th>Area</th>
        <th>Budget</th>
        <th>Tipo de propiedad</th>
        <th>Oferta cotizada</th>
        <th>Registrada</th>
        <th>Actualizada</th>
        </thead>
        <tbody>
        <tr>
            <td>{{ $quoting->id }}</td>
            <td>{{ $quoting->full_name }}</td>
            <td>{{ $quoting->phone }}</td>
            <td><a href="mailto:{{ $quoting->email }}">{{ $quoting->email }}</a></td>
            <td>{{ $quoting->area }} m2</td>
            <td>$ {{ $quoting->budget }}</td>
            <td>{{ $quoting->property_type }}</td>
            @if( $quoting->final_price)
                <td>$ {{ $quoting->final_price }}</td>
            @else
                <td class="text-info">Sin asignar</td>
            @endif
            <td>{{ $quoting->created_at->diffForHumans() }}</td>
            <td>{{ $quoting->updated_at->diffForHumans() }}</td>
        </tr>
        </tbody>
    </table>
    <hr>
</div>

<form action="{{ route('quotings.update',$quoting->id) }}" method="POST">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}

    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label for="name">Precio de oferta</label>
                    <input type="number" placeholder="Precio de cotización" class="form-control" name="final_price" required >
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <label for="name">Estado de la cotización</label>
                    <select name="status"  class="form-control">
                        <option value="ATENDIDA" selected>Atendida</option>
                        <option value="VENTA">Venta realizada</option>
                        <option value="RECHAZADA">Rechazada por el lead</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <a href="{{ route('quotings.index') }}" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </div>
    </div>
</form>

<div class="card-body table-responsive p-0">
    <table class="table table-hover table-bordered">
        <thead>
        <th>ID</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Email</th>
        <th>Area</th>
        <th>Budget</th>
        <th>Tipo de propiedad</th>
        <th>Oferta cotizada</th>
        <th>Registrada</th>
        <th>Actualizada</th>
        </thead>
        <tbody>
        <tr>
            <td>{{ $quoting->id }}</td>
            <td>{{ $quoting->full_name }}</td>
            <td>{{ $quoting->phone }}</td>
            <td><a href="mailto:{{ $quoting->email }}">{{ $quoting->email }}</a></td>
            <td>{{ $quoting->area }} m2</td>
            <td>$ {{ $quoting->budget }}</td>
            <td>{{ $quoting->property_type }}</td>
            @if( $quoting->final_price)
                <td>$ {{ $quoting->final_price }}</td>
            @else
                <td class="text-info">Sin asignar</td>
            @endif
            <td>{{ $quoting->created_at->diffForHumans() }}</td>
            <td>{{ $quoting->updated_at->diffForHumans() }}</td>
        </tr>
        </tbody>
    </table>
    <hr>
</div>

<p class="p-3 text-muted">
  <strong>Mensaje adicional:</strong>  {{ $quoting->comment }}
</p>

<a href="{{ route('quotings.index') }}" class="btn btn-secondary ml-3 mb-3">Volver</a>

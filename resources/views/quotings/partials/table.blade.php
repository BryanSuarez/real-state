<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">Cotizaciones Pendientes</h3>

                    <div class="card-tools">
                        <form action="{{ route('quotings.index') }}" method="GET">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="searchText" class="form-control float-right" placeholder="Buscar registro"
                                       value="{{ $query }}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                        @foreach($quotings as $quoting)
                            <tr>
                                <td>{{ $quoting->id }}</td>
                                <td>{{ $quoting->full_name }}</td>
                                @if($quoting->status === 'CREADA')
                                    <td class="text-danger">
                                        Pendiente de atender
                                    </td>
                                @endif
                                <td>
                                    <a href="{{ route('quotings.show', $quoting->id) }}" class="btn btn-outline-info">Detalles</a>
                                    <a href="{{ route('quotings.edit', $quoting->id) }}" class="btn btn-primary">Editar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $quotings->links() }}
                </div>
            </div>
            <!-- /.card  pending-->
        </div>
        <div class="col-md-6">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Cotizaciones Atendidas</h3>

                    <div class="card-tools">
                        <form action="{{ route('quotings.index') }}" method="GET">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="searchText" class="form-control float-right" placeholder="Buscar registro"
                                       value="{{ $query }}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                        @foreach($vieweds as $quoting)
                            <tr>
                                <td>{{ $quoting->id }}</td>
                                <td>{{ $quoting->full_name }}</td>
                                @if($quoting->status === 'ATENDIDA')
                                    <td class="text-success">
                                        Atendida
                                    </td>
                                @endif
                                <td>
                                    <a href="{{ route('quotings.show', $quoting->id) }}" class="btn btn-outline-info">Detalles</a>
                                    <a href="{{ route('quotings.edit', $quoting->id) }}" class="btn btn-primary">Editar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $vieweds->links() }}
                </div>
            </div>
            <!-- /.card  pending-->
        </div>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Ventas realizadas</h3>

                    <div class="card-tools">
                        <form action="{{ route('quotings.index') }}" method="GET">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="searchText" class="form-control float-right" placeholder="Buscar registro"
                                       value="{{ $query }}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                        @foreach($sales as $quoting)
                            <tr>
                                <td>{{ $quoting->id }}</td>
                                <td>{{ $quoting->full_name }}</td>
                                @if($quoting->status === 'VENTA')
                                    <td class="text-success">
                                        Venta realizada
                                    </td>
                                @endif
                                <td>
                                    <a href="{{ route('quotings.show', $quoting->id) }}" class="btn btn-outline-info">Detalles</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $vieweds->links() }}
                </div>
            </div>
            <!-- /.card  pending-->
        </div>
    </div>
</div>
